/**
 *  \file parallel-mergesort--omp.cc
 *
 *  \brief Implement your parallel mergesort in this file.
 */

#include <assert.h>
#include <stdio.h>
#include <math.h>
/**
 *  *  \file parallel-mergesort--omp.cc
 *   *
 *    *  \brief Implement your parallel mergesort in this file.
 *     */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "sort.hh"

int binsearch(int x, keytype *T, int p, int r)
{
  int low=p,mid,high;
  high = (p > (r+1)) ? p : (r+1);
  while(low<high)
  {
	mid=(low+high)/2;
	if(x<=T[mid])
	high=mid;
	else
	low=mid+1;
  }
 return high;
}
void merge(keytype *T, int p1, int r1, int p2, int r2, keytype *A, int p3)
{

 int temp;
 int n1=(r1-p1)+1;
 int n2=(r2-p2)+1;
 if(n1<n2)
 {
   temp=n1;
   n1=n2;
   n2=temp;
   temp=r1;
   r1=r2;
   r2=temp;
   temp=p1;
   p1=p2;
   p2=temp;
 }
 else if(n1==0)
   return;
 else
 {
  	int q1=(p1+r1)/2;
	int q2=binsearch(T[q1],T,p2,r2);
	int q3=p3+(q1-p1)+(q2-p2);
 	A[q3]=T[q1];
	#pragma omp task
	merge(T,p1,q1-1,p2,q2-1,A,p3);
	#pragma omp task
	merge(T,q1+1,r1,q2,r2,A,q3+1);
	#pragma omp taskwait
 }
}

void mergesort(keytype *A, int p, int r,keytype *B,int s)
{
  int n=r-p+1;
  if(n==1) 
   B[s]=A[p];
  else
  {
    int q=(p+r)/2;
    int q1=q-p+1;
   int i;
   keytype* T=A;
   #pragma omp task 
   mergesort(A,p,q,T,1);

   //for(i=0;i<mid;i++)
  // *(B+i)=*(A+i);
   // p=i;
   #pragma omp task
   mergesort(A,q+1,r,T,q1+1);
   #pragma omp taskwait 
  merge(T,1,q1,q1+1,n,B,s);    
  }

}

void
parallelSort (int N, keytype* A)
{
keytype *B = A;
#pragma omp parallel
#pragma omp single	
mergesort(A,0,N-1,B,0);
  /* Lucky you, you get to start from scratch */
}

/* eof */	
