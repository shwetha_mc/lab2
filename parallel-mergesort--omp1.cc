/**
 *  \file parallel-mergesort--omp.cc
 *
 *  \brief Implement your parallel mergesort in this file.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "sort.hh"

void merge(int N, keytype *A, int n1, keytype *B, int n2, keytype *C)
{
  int i=0,j=0,k=0;
  while(i<n1 && j<n2)
  {
	if(*(B+i) < *(C+j))
	{
	   *(A+k)=*(B+i);
	   ++k;
	   ++i;
	}
	else
	{
	   *(A+k)=*(C+j);
	   ++k;
	   ++j;
	}
	
  }
  if(i==n1)
  {
	while(k<N)
	{
	  *(A+k)=*(C+j);
	  ++k;
	  ++j;
	}
  }
  else
  {
	while(k<N)
	{
	  *(A+k)=*(B+i);
   	  ++k;
	  ++i;
	}
  }	
}

void mergesort(int N,keytype *A)
{
  int G=1024;
  if(N<=G)
  sequentialSort(N,A);
  else 
  {
  int mid=N/2,j,i,p,q;
  keytype *B=newKeys(mid), *C=newKeys(N-mid);
 // #pragma omp task
//
// default(none) shared(A,mid,B,i,p)
 // {
  #pragma omp for private(i)// shared(B,A,mid,p) private (i)
  for(i=0;i<mid;i++)
  *(B+i)=*(A+i);
  p=i;
 // }
 // #pragma omp task
 // {
  i=0;
  #pragma omp for private(i,j) //shared(N,mid,A,C,q) private(i,j)
  for(j=mid;j<N;j++)
  {
  
  *(C+i)=*(A+j);
  ++i;
  }
  q=i;
//  }
//  #pragma omp taskwait
  #pragma omp task 
//default(none) shared(p,B)
  mergesort(p,B);
  mergesort(q,C);
  #pragma omp taskwait
  merge(N,A,p,B,q,C);
  }
}


void
parallelSort (int N, keytype* A)
{
#pragma omp parallel default(none) shared(N,A)
#pragma omp single  
  mergesort(N,A);	
  /* Lucky you, you get to start from scratch */
}

/* eof */
